name 'stegosoc'
maintainer 'Mayank Gaikwad'
maintainer_email 'mayank.gaikwad@talentica.com'
license 'All Rights Reserved'
description 'Installs/Configures stegosoc'
long_description 'Installs/Configures stegosoc'
version IO.read(File.join(File.dirname(__FILE__), 'VERSION')).chomp rescue '0.0.1'
chef_version '>= 12.1' if respond_to?(:chef_version)

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
issues_url 'https://bitbucket.org/mayankg-tal/stegosoc/issues'

# The `source_url` points to the development reposiory for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
source_url 'https://mayankg-tal@bitbucket.org/mayankg-tal/stegosoc'

depends 'apt'
depends 'yum'
depends 'ssm_agent'
depends 'logrotate'

supports 'ubuntu'
supports 'centos'
supports 'amazon'
